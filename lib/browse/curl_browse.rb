require 'curb'
require 'pry'
require 'nokogiri'

class CurlBrowse
  def page_data(url)
    data_http = special_request(url)
    while data_http.redirect_url != nil do
      data_http = special_request(data_http.redirect_url)
    end
    i = 0
    time = 0
    while (data_http.status == '404 Not Found' && i < 3_600)do
      puts '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@- 404 Not Found -@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
      puts data_http.status
      case time
        when 0..4          then i = 1
        when 5..29         then i = 5
        when 30..59        then i = 10
        when 60..599       then i = 60
        when 600..3_599    then i = 600
        when 3_600..86_400 then i = 3_600
      end
      print 'sleep time: '
      p time += 1*i
      sleep(time)
      data_http = special_request(data_http.redirect_url)
    end
    [Nokogiri::HTML(data_http.body_str), data_http.url, data_http]
  end

  private

  def special_request(url)
    i = 0
    j = 0
    begin
      user_agents = ['Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:39.0) Gecko/20100101 Firefox/39.0',
                     'Mozilla/5.0 (Windows NT 6.1; rv:35.0) Gecko/20100101 Firefox/35.0',
                     'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)
                      Chrome/44.0.2403.155 Safari/537.36']
      puts "URL before open: #{url}"
      data_http = Curl::Easy.http_get(url) do |easy|
        easy.ssl_verify_peer = false
        easy.useragent       = user_agents[rand(user_agents.size)]
        easy.cookies         = cookies_to_s
        # easy.verbose         = true
        easy.timeout         = 180
      end
    rescue Curl::Err::HostResolutionError => error
      sleep(1*5**i)
      i += 1
      if i < 5
        retry
      else
        raise
      end
    rescue Curl::Err::TimeoutError => error
      j +=1
      if j < 10
        retry
      else
        raise
      end
    rescue => error
      raise
    ensure
      if error
        $errors = report_error("#{Time.now} - #{error.class} and #{error.message}")
        log_errors
        File.open('cookie', 'w') { |f| f.write($cookies_hash) }
      end
    end
    print 'data_http.status: '
    puts data_http.status
    print 'data_http.url: '
    puts data_http.url
    cookie_save(data_http)
    data_http
  end

  def report_error(error_message)
    (Thread.current[:errors] ||= []) << "#{error_message}"
  end

  def log_errors
    File.open('errors.txt', 'a') do |file|
      ($errors ||= []).each do |error|
        file.puts error
      end
    end
  end

  def cookie_save(data_http)
    header = data_http.header_str
    cookie_string_array = header.scan(/Set\-Cookie\: .*?\r\n/)
    cookie_string_array.each do |string|
      str_arr = string[12..-3].split('; ')
      str_arr.each do |cookie|
        key, value = cookie.split('=', 2)
        $cookies_hash[key] = value
      end
    end
  end

  def cookies_to_s
    cookies_string = ''
    $cookies_hash.each do |key, value|
      if value == nil
        cookies_string += "#{key}; "
      else
        cookies_string += "#{key}=#{value}; "
      end
    end
    cookies_string
  end
end
