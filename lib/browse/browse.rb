require 'csv'
require 'pry'
require_relative 'curl_browse'
require_relative '../captcha/captcha'
require_relative '../errors/browse_page_url_empty_error'
#require_relative 'phantomjs_browse'

class Browse
  def page(url_0)

    binding.pry if ( url_0.include?('help?article') || url_0.include?('yandex.ru/support') )
    1/0 if ( url_0.include?('help?article') || url_0.include?('yandex.ru/support') )

    raise BrowsePageUrlEmptyError if url_0 == ROOT_URL
    array = my_browse(url_0)
    while ( array == [] && i < 3_600 ) do
      puts '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@- EMPTY -@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
      case time
        when 0..4          then i = 1
        when 5..29         then i = 5
        when 30..59        then i = 10
        when 60..599       then i = 60
        when 600..3_599    then i = 600
        when 3_600..86_400 then i = 3_600
      end
      print 'sleep time: '
      p time += 1*i
      sleep(time)
      array = my_browse(url_0)
    end
    url_1 = array[1]
    if url_1.include? 'captcha'
      array = define_sleep_time_for_captcha(url_0, url_1)
      url_2 = array[1]
      if url_2.include? 'captcha'
        Captcha.new.captcha_save(url_2)
      end
    end
    array[0]
  end

  def page_for_captcha(url)
    array = my_browse(url)
    array[0]
  end

  def page_curl(url)
    array = my_browse(url)
    array[2]
  end

  private

  def my_browse(url)
    CurlBrowse.new.page_data(url)
    #PhantomjsBrowse.new.phaeton_browse(url)
  end

  def define_sleep_time_for_captcha(url_0, url_1)
    array = []
    url_2 = url_1
    i = 0
    time = 0
    #60*60*24 = 86_400
    while ((url_2.include?('captcha') || !array) && (time < 86_401))
      puts '@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@- captcha -@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@'
      puts url_0
      puts url_2
      puts DateTime.now
      puts Time.now
      case time
        when 0..4          then i = 1
        when 5..29         then i = 5
        when 30..59        then i = 10
        when 60..599       then i = 60
        when 600..3_599    then i = 600
        when 3_600..86_400 then i = 3_600
      end
      print 'sleep time: '
      p time += 1*i
      CSV.open('sleep-captcha-time.csv', 'a+') do |csv|
        csv << [Time.now, time, i, url_0, url_2]
      end
      sleep(time)
      array = my_browse(url_0)
      url_2 = array[1]
    end
  end

end