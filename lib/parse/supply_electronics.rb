# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../models/supply'
require_relative '../errors/supply_electronics_product_id_error'
require_relative '../errors/supply_electronics_shop_id_error'
require_relative '../errors/shop_electronics_error'

class SupplyElectronics

  def parse(page, a_lot_of_attributes, array_product_id, shops_ids, id_product = nil)
    if a_lot_of_attributes
      product_id = id_product
      shop_product_name = page.xpath("//span[@class='snippet-card__header-text']").map(&:text)
      shop_descriptions = page.xpath("//div[@class='snippet-card__desc snippet-card__desc_nowrap_yes']").map(&:text)
    else
      product_id = array_product_id[0]
      shop_product_name = []
      shop_descriptions = []
    end
    prices = page.xpath("//div[@class='snippet-card__price']").map(&:text)
    tags_a_for_old_prices = page.xpath("//a[@class='link offer__price_visibility_toggle shop-history__link']")
    divs_delivery           = page.xpath("//div[@class='snippet-card__info ']/div")
    array_of_hashes_supply = []
    divs_delivery.each_with_index do |div_deliv, i|
      product_id += 1 if !a_lot_of_attributes
      shop_description = shop_descriptions[i]
      shop_description = nil if shop_description == ''
      price         = price_to_i(prices[i])
      old_price     = nil_or_old_price(tags_a_for_old_prices[i])
      delivery      = div_deliv.xpath("./div[span[@class='delivery__icon delivery__icon_type_truck']]").text
      delivery_cost = delivery_cost_to_i(delivery)
      in_stock_flag = in_stock_define(delivery)
      self_delivery        = div_deliv.xpath("./div[span[@class='delivery__icon delivery__icon_type_self']]").text
      if self_delivery == ''
        self_delivery = nil
        self_delivery_possibility_flag = nil
      else
        self_delivery_possibility_flag = self_delivery_possibility_define(self_delivery)
      end
      delivery_description = div_deliv.xpath("./div/div[position()=1]").text
      delivery_description = nil if delivery_description == ''
      array_of_hashes_supply << { product_id: product_id, shop_id: shops_ids[i],
                                  shop_product_name: shop_product_name[i],
                                  shop_product_description: shop_description,
                                  price: price, old_price: old_price,
                                  delivery_cost: delivery_cost, delivery: delivery,
                                  in_stock: in_stock_flag, self_delivery: self_delivery,
                                  self_delivery_possibility: self_delivery_possibility_flag,
                                  delivery_description: delivery_description }
      puts "price: #{price}"
    end
    raise SupplyElectronicsProductIdError if (!a_lot_of_attributes && product_id != array_product_id[1])
    raise SupplyElectronicsShopIdError if divs_delivery.length != shops_ids.length
    Supply.dataset.multi_insert(array_of_hashes_supply)
  end

  private

  def price_to_i(text_price)
    t_price = text_price[0..-6]
    space   = t_price.gsub(/\d/, '')[0..0]
    t_price.gsub(space, '').to_i
  end

  def nil_or_old_price(tag_a)
    str_old_price = tag_a.xpath("./div/div[@class='price price_unactual_yes metrika i-bem']").text
    str_old_price.empty? ? nil : price_to_i(str_old_price)
  end

  def delivery_cost_to_i(delivery)
    if delivery.rindex(/\d/)
      delivery_cost = delivery[0..delivery.rindex(/\d/)].to_i
      raise SupplyElectronicsError if delivery_cost < 0
    else
      if delivery[0..8] == 'Бесплатно'
        delivery_cost = 0
      else
        delivery_cost = nil
      end
    end
    delivery_cost
  end

  def in_stock_define(delivery)
    text = delivery[delivery.rindex(',')+1..-1].strip
    if text == 'в наличии'
      in_stock_flag = true
    elsif text == 'на заказ'
      in_stock_flag = false
    else
      in_stock_flag = nil
    end
    in_stock_flag
  end

  def self_delivery_possibility_define(self_delivery)
    text = self_delivery.strip
    if text == 'Самовывоз'
      self_delivery_possibility_flag = true
    else
      self_delivery_possibility_flag = nil
    end
    self_delivery_possibility_flag
  end
end
