# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../errors/clothes_pages_error'

class ClothesPages

  def parse(page)
    array = parse_one_page(page)
    urls       = array[0]
    names      = array[1]
    prices     = array[2]
    old_prices = array[3]
    images     = array[4]
    div_pages  = array[5]
    if !div_pages.empty?
      divs = div_pages.xpath("./a/@href")
      url = divs[0].text
      if !url.empty?
        raise ClothesPagesError if url[-7..-1] != '&page=2'
        url_0 = url[0..-2]
        i = 2
        max_page_number = 2
        flag_next_page = true
        while flag_next_page
          url_n = "#{url_0}#{i}"
          next_page = Browse.new.page(ROOT_URL + url_n)
          array = parse_one_page(next_page)
          urls       += array[0]
          names      += array[1]
          prices     += array[2]
          old_prices += array[3]
          images     += array[4]
          div_pages   = array[5]
          if max_page_number == i
            href = div_pages.xpath("./a[position()=last()]/@href").text
            last_page_number_link = href.split('=')[-1].to_i
            if last_page_number_link > max_page_number
              max_page_number = last_page_number_link
            else
              flag_next_page = false
            end
          end
          i += 1
        end
      end
    end
    [urls, names, prices, old_prices, images]
  end

  private

  def parse_one_page(page)
    tags_a = page.xpath("//div[@class='b-wear-search-catalog__list']/div/a[@class='b-link']")
    urls       = []
    names      = []
    prices     = []
    old_prices = []
    images     = []
    tags_a.each do |a|
      url = a.xpath("./@href").text
      url = ROOT_URL + url if !( url.include?(ROOT_URL) || url.include?('http://help.yandex.ru') )
      urls       << url
      names      << a.xpath("./div[@class='b-model__title']").text
      prices     << a.xpath("./div/span[@class='b-prices b-prices_min b-prices_sale_new']/
                               span[@class='b-prices__num']").text
      old_prices << a.xpath("./div/span[@class='b-prices b-prices_min b-prices_sale_old']/
                               span[@class='b-prices__num']").text
      images     << a.xpath("./div[@class='b-model__image']/img/@src").text
    end
    div_pages = page.xpath("//div[@class='b-pager__pages']")
      [urls, names, prices, old_prices, images, div_pages]
  end
end
