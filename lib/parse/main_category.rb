# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'csv'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../models/category'
require_relative 'category_parse'

class MainCategory
  def parse_manage
    data_array = parse
    data_array.each do |name, url|
      print name
      print ' '
      puts  full_url = ROOT_URL + url
      puts  '=' * 100
      parse_down(name, full_url)
    end
  end

  private

  def parse
    catalog_page = Browse.new.page("#{ROOT_URL}/catalog")
    tag_div_main = catalog_page.xpath("/html/body/div")
    tag_div      = tag_div_main.xpath("./div/noindex/ul/li[position()=last()]/div/div")
    tag_a        = tag_div.xpath("./a")  #[position()>4] - for clothes
    names        = tag_a.map(&:text)
    urls         = tag_a.xpath("./@href").map(&:text)
    data_array   = names.zip(urls)
    last_changeable_data = tag_div_main.xpath("./div/div/div/div/
                                               div[@class='catalog-simple__item'][position()=last()]")
    last_changeable_name = last_changeable_data.xpath("./a/h2").text
    last_changeable_url  = last_changeable_data.xpath("./div[@class='catalog-simple__footer']/a/@href").text
    data_array          << [last_changeable_name, last_changeable_url]
  end

  def parse_down(name, url)
    parent_id = 0
    category  = Category.new(name: name, parent_id: parent_id, url: url)
    category.save
    id   = category.id
    page = Browse.new.page(url)
    CategoryParse.new.parse(url, page, id)
  end
end
