# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative 'shop_electronics'
require_relative 'supply_electronics'
require_relative 'photo_electronics'
require_relative '../models/product'
require_relative '../models/attribute_name'
require_relative '../models/attribute_value'
require_relative '../browse/browse'
require_relative 'description_electronics'
require_relative 'attribute_electronics'
require_relative '../errors/electronics_producer_error'
require_relative '../errors/electronics_attributes_error'

class ElectronicsParseDown
  def product_parse(page, id_category, a_lot_of_attributes, id_producer)
    names = page.xpath("//span[@class='snippet-card__header-text']").map(&:text)
    if a_lot_of_attributes
      tags_div_ratings = page.xpath("//div[@class='snippet-card__view']")
    else
      product_rating = nil
    end
    array_of_hashes_products = []
    h3 = page.xpath("//h3")
    h3.each_with_index do |h, i|
      if a_lot_of_attributes
        flag_new = !h.xpath("./div/div").empty?
        product_rating = tags_div_ratings[i].xpath("./div[@class='rating rating_outline_yes hint i-bem']").text
        product_rating = nil if product_rating = ''
      else
        flag_new = nil
      end
      array_of_hashes_products << { category_id: id_category, producer_id: id_producer,
                                    name: names[i], rating: product_rating,
                                    new_one: flag_new, a_lot_of_attributes: a_lot_of_attributes }
      puts names[i]
      puts '-'*20
    end
    last_product = Product.dataset.order(:id).last
    if last_product
      id_previous = last_product.id
    else
      id_previous = 0
    end
    Product.dataset.multi_insert(array_of_hashes_products)
    id_last = Product.dataset.order(:id).last.id
    [id_previous, id_last]
  end
end
