# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../models/supply'
require_relative '../errors/supply_clothes_several_arrays_length_error'
require_relative '../errors/supply_clothes_detailed_parse_error'

class SupplyClothes

  def parse_one_supply(product_page, product_id, shop_id, price_string, old_price_string)
    tag_div = product_page.xpath("//
              div[@class='offer__out-stock offer__out-stock-message offer__out-stock-text_offset_top']")
    delivery = tag_div.xpath("./div/div[span[@class='delivery__icon delivery__icon_type_truck']]").text
    delivery = nil if delivery == ''
    if delivery
      delivery_cost = delivery_cost_to_i(delivery)
    else
      delivery_cost = nil
    end
    delivery_description = tag_div.xpath("./div/div/div[@class='delivery__popup-info'][1]").text
    delivery_description = nil if delivery_description == ''
    shop_product_description = tag_div.xpath("./div/div/div[@class='delivery__popup-info'][2]").text
    tags_dl = tag_div.xpath("./div/div[@class='product-card-offer__spec']/dl")
    if tags_dl != []
      shop_product_description << " | " if shop_product_description != ''
      tags_dl.each do |dl|
        array = detailed_parse(dl)
        name         = array[0]
        values_array = array[1]
        values = ''
        values_array.each do |value|
          value.gsub!("\n", '')
          value.gsub!("  ", '')
          value.strip!
          values << "#{value}, "
        end
        values = values[0..-3]
        shop_product_description << "#{name} #{values} | "
      end
    end
    if shop_product_description == ''
      shop_product_description = nil
    else
      shop_product_description = shop_product_description[0..-4]
    end
    price = price_to_i(price_string)
    puts "price: #{price}"
    old_price = price_to_i(old_price_string)
    { product_id: product_id, shop_id: shop_id,
      shop_product_description: shop_product_description,
      price: price, old_price: old_price, delivery_cost: delivery_cost,
      delivery: delivery, delivery_description: delivery_description }
  end

  def parse_several_supply(array_of_hashes_supply, page_attr, product_id, array_ids_shops)
    delivery = []
    delivery_cost = []
    shop_product_description_array = []
    delivery_descriptions = []
    prices = []
    old_prices = []
    tags_div = page_attr.xpath("//div[@class='product-top-offer shop-history i-bem product-top-offer_type_group']")
    tags_div.each do |tag_div|
      shop_product_description = ''
      div_delivery_i_bem = tag_div.xpath("./div/
                                   div[@class='product-top-offer__item product-top-offer__item_type_delivery']/div")
      deliv = div_delivery_i_bem.xpath("./div[@class='delivery__info']/text()[1]").text
      delivery << deliv
      delivery_cost << delivery_cost_to_i(deliv)
      product_description = div_delivery_i_bem.xpath("./div[@class='delivery__info']/div").text
      shop_product_description << "#{product_description} | " if product_description != ''

      del_descr = div_delivery_i_bem.xpath("./div[@class='delivery__popup']/div").map(&:text)
      if del_descr == []
        delivery_description = nil
      else
        delivery_description = ''
        del_descr.each do |del_descr|
          delivery_description << "#{del_descr} | " if del_descr != ''
        end
      end
      delivery_description = delivery_description[0..-4]
      delivery_descriptions << delivery_description
      tags_dl = tag_div.xpath("./div/
        div[@class='product-top-offer__item product-top-offer__item_type_spec product-top-offer__item_spec-amount_big']/
        dl")
      tags_dl.each do |dl|
        array = detailed_parse(dl)
        name         = array[0]
        values_array = array[1]
        values = ''
        values_array.each do |value|
          value.gsub!("\n", '')
          value.gsub!("  ", '')
          value.strip!
          values << "#{value}, "
        end
        values = values[0..-3]
        shop_product_description << "#{name} #{values} | "
      end
      price_string = tag_div.xpath("./div/div[@class='product-top-offer__item product-top-offer__item_type_price']/
                                    a/div/div[@class='price price_discount_yes']").text
      if price_string == ''
        price_string = tag_div.xpath("./div/
                               div[@class='product-top-offer__item product-top-offer__item_type_price']/a").text
        old_price = nil
      else
        old_price_string = tag_div.xpath("./div/
                                          div[@class='product-top-offer__item product-top-offer__item_type_price']/
                                          a/div/div[@class='price price_unactual_yes metrika i-bem']").text
        old_price = price_to_i(old_price_string)
      end
      prices << price_to_i(price_string)
      old_prices << old_price
      shop_product_description = shop_product_description[0..-4]
      shop_product_description_array << shop_product_description
    end
    raise SupplyClothesSeveralArraysLengthError if array_ids_shops.length != delivery.length
    array_ids_shops.each_with_index do |shop_id, i|
      array_of_hashes_supply << { product_id: product_id, shop_id: shop_id,
                                  shop_product_description: shop_product_description_array[i],
                                  price: prices[i], old_price: old_prices[i],
                                  delivery_cost: delivery_cost[i], delivery: delivery[i],
                                  delivery_description: delivery_descriptions[i] }
      puts "price: #{prices[i]}"
    end
    array_of_hashes_supply
  end

  private

  def delivery_cost_to_i(delivery)
    if delivery.rindex(/\d/)
      delivery_cost = delivery[0..delivery.rindex(/\d/)].to_i
    else
      if delivery[0..8] == 'Бесплатно'
        delivery_cost = 0
      else
        delivery_cost = nil
      end
    end
    delivery_cost
  end

  def price_to_i(price_string)
    text_price = price_string.gsub(/\D/, '')
    space      = text_price.gsub(/\d/, '')[0..0]
    text_price.gsub!(space, '')
    text_price.to_i
  end

  def nil_or_old_price(tag_a)
    str_old_price = tag_a.xpath("./div/div[@class='price price_unactual_yes metrika i-bem']").text
    str_old_price.empty? ? nil : price_to_i(str_old_price)
  end

  def detailed_parse(dl)
    name = dl.xpath("./dt/span").text
    span = dl.xpath("./dd/span")
    color = span.xpath("./param/@name").text
    values_array = []
    case color
      when 'Цвет'
        divs = span.xpath("./div/div")
        divs.each do |div|
          values_array << div.xpath("./@title").text
        end
      when ''
        values_array << span.text
      else
        raise SupplyClothesDetailedParseError
    end
    [name, values_array]
  end
end
