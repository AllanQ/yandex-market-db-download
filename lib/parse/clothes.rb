# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative 'clothes_pages'
require_relative 'shop_clothes'
require_relative 'supply_clothes'
require_relative 'photo_clothes'
require_relative 'attribute_clothes'
require_relative '../browse/browse'
require_relative '../models/product'
require_relative '../models/shop'
require_relative '../models/supply'
require_relative '../models/photo'
require_relative '../models/attribute_name'
require_relative '../models/attribute_value'

class Clothes

  def parse(page, id_category)
    array_pages_info = ClothesPages.new.parse(page)
    urls       = array_pages_info[0]
    names      = array_pages_info[1]
    prices     = array_pages_info[2]
    old_prices = array_pages_info[3]
    images     = array_pages_info[4]
    array_of_hashes_products    = []
    array_of_hashes_shops       = []
    shops_from_hashes           = []
    shops_ids_from_hashes       = []
    array_of_hashes_supply      = []
    array_of_hashes_photos      = []
    array_of_hashes_attr_names  = []
    attr_names_from_hashes      = []
    attr_names_ids_from_hashes  = []
    array_of_hashes_attr_values = []
    id_product      = id_product_define
    id_shop_index   = ShopClothes.new.id_define
    id_photo        = PhotoClothes.new.id_define
    id_attr_name_i  = AttributeClothes.new.id_name_define
    urls.each_with_index do |url, i|
      product_page = Browse.new.page(url)
      all_characteristics_url = product_page.xpath("//div[@class='product-card__spec-text-more']/a/@href").text
      if all_characteristics_url == ''
        array_of_hashes_products << { category_id: id_category, name: names[i], a_lot_of_attributes: false }
        id_product += 1
        puts names[i]
        puts '-'*20
        page_supply = page
      else
        array_of_hashes_products << { category_id: id_category, name: names[i], a_lot_of_attributes: true }
        id_product += 1
        puts names[i]
        puts '-'*20
        url_attributes = ROOT_URL + all_characteristics_url
        page_supply = Browse.new.page(url_attributes)
      end
      array_photo = PhotoClothes.new.parse(array_of_hashes_photos, id_photo, product_page, images[i], id_product)
      array_of_hashes_photos = array_photo[0]
      id_photo               = array_photo[1]
      array_shop = ShopClothes.new.parse_one_shop(array_of_hashes_shops, id_shop_index, product_page,
                                                  shops_from_hashes, shops_ids_from_hashes)
      array_of_hashes_shops  = array_shop[0]
      id_shop                = array_shop[1]
      id_shop_index          = array_shop[2]
      one_shop_name          = array_shop[3]
      shops_from_hashes      = array_shop[4]
      shops_ids_from_hashes  = array_shop[5]
      array_shops_names = page_supply.xpath("//div[@class='product-top-offers-list metrika i-bem']/
                                             div/div/div/div/a[@class='link link_outer_yes']").map(&:text)
      if array_shops_names == []
        array_of_hashes_supply << SupplyClothes.new.parse_one_supply(product_page, id_product,
                                                                     id_shop, prices[i], old_prices[i])
      else
        shops = array_shops_names.uniq - [one_shop_name]
        if shops == []
          array_ids_shops = []
          array_shops_names.length.times do
            array_ids_shops << id_shop
          end
        else
          array_shop = ShopClothes.new.parse_several_shops(array_of_hashes_shops, id_shop_index, page_supply,
                                                           shops, array_shops_names, one_shop_name, id_shop,
                                                           shops_from_hashes, shops_ids_from_hashes)
          array_of_hashes_shops = array_shop[0]
          id_shop_index         = array_shop[1]
          array_ids_shops       = array_shop[2]
          shops_from_hashes     = array_shop[3]
          shops_ids_from_hashes = array_shop[4]
        end
        array_of_hashes_supply = SupplyClothes.new.parse_several_supply(array_of_hashes_supply, page_supply,
                                                                        id_product, array_ids_shops)
      end
      if all_characteristics_url == ''
        array_attr = AttributeClothes.new.parse_from_product_page(product_page, id_category, id_product,
                                                                  array_of_hashes_attr_names,
                                                                  array_of_hashes_attr_values, id_attr_name_i,
                                                                  attr_names_from_hashes, attr_names_ids_from_hashes)
        array_of_hashes_attr_names  = array_attr[0]
        array_of_hashes_attr_values = array_attr[1]
        id_attr_name_i              = array_attr[2]
        attr_names_from_hashes      = array_attr[3]
        attr_names_ids_from_hashes  = array_attr[4]
      else
        array_attr = AttributeClothes.new.parse_from_separate_page(page_supply, id_category, id_product,
                                                                   array_of_hashes_attr_names,
                                                                   array_of_hashes_attr_values, id_attr_name_i,
                                                                   attr_names_from_hashes, attr_names_ids_from_hashes)
        array_of_hashes_attr_names  = array_attr[0]
        array_of_hashes_attr_values = array_attr[1]
        id_attr_name_i              = array_attr[2]
        attr_names_from_hashes      = array_attr[3]
        attr_names_ids_from_hashes  = array_attr[4]
      end
    end
    Product.dataset.multi_insert(array_of_hashes_products)
    Shop.dataset.multi_insert(array_of_hashes_shops) if array_of_hashes_attr_names != []
    Supply.dataset.multi_insert(array_of_hashes_supply)
    Photo.dataset.multi_insert(array_of_hashes_photos)
    AttributeName.dataset.multi_insert(array_of_hashes_attr_names) if array_of_hashes_attr_names != []
    AttributeValue.dataset.multi_insert(array_of_hashes_attr_values) if array_of_hashes_attr_values != []
  end

  private

  def id_product_define
    last_product = Product.dataset.order(:id).last
    if last_product
      id_product = last_product.id
    else
      id_product = 0
    end
    id_product
  end
end
