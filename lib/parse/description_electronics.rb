# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../models/attribute_name'
require_relative '../models/attribute_value'
require_relative '../errors/description_electronics_product_id_error'
require_relative '../errors/description_electronics_shop_id_error'

class DescriptionElectronics

  def attribute_name(id_category)
    record = AttributeName.find(category_id: id_category, parent_id: 0, name: 'Описание')
    if record
      id = record.id
    else
      attribute_name = AttributeName.new(category_id: id_category, parent_id: 0, name: 'Описание')
      attribute_name.save
      id = attribute_name.id
    end
    id
  end

  def attribute_value(page, a_lot_of_attributes, id_attribute_name_description,
                                  array_product_id, shops_ids, id_product = nil)
    descriptions = page.xpath("//div[@class='snippet-card__desc snippet-card__desc_nowrap_yes']").map(&:text)
    descriptions_additional = page.xpath("//div[@class='snippet-card__col']")
    if a_lot_of_attributes
      product_id = id_product
    else
      product_id = array_product_id[0]
    end
    array_of_hashes_attributes_values = []
    descriptions.each_with_index do |descr, i|
      product_id += 1 if !a_lot_of_attributes
      if descr != ''
        array_of_hashes_attributes_values << { product_id: product_id, shop_id: shops_ids[i],
                                               attributes_names_id: id_attribute_name_description, value: descr }
      end
      descr_add = descriptions_additional[(i+1)*2-1].xpath("./div[@class='snippet-card__shop-info']").map(&:text)
      if descr_add != []
        descr_add.each do |des|
          array_of_hashes_attributes_values << { product_id: product_id, shop_id: shops_ids[i],
                                                 attributes_names_id: id_attribute_name_description, value: des }
        end
      end
    end
    raise DescriptionElectronicsProductIdError if (!a_lot_of_attributes && product_id != array_product_id[1])
    raise DescriptionElectronicsShopIdError if descriptions.length != shops_ids.length
    AttributeValue.dataset.multi_insert(array_of_hashes_attributes_values)
  end
end
