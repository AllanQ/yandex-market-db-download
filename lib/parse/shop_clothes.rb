# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../models/shop'
require_relative '../browse/browse'
require_relative '../errors/shop_clothes_error'
require_relative '../errors/shop_clothes_name_error'
require_relative '../errors/shop_clothes_id_error'

class ShopClothes

  def id_define
    last_shop = Shop.dataset.order(:id).last
    if last_shop
      id = last_shop.id
    else
      id = 0
    end
    id
  end

  def parse_one_shop(array_of_hashes_shops, id_shop_index, product_page, shops_from_hashes, shops_ids_from_hashes)
    tag_div = product_page.xpath("//div[@class='offer__out-stock offer__out-stock-message offer__out-stock-text_offset_top']")
    shop = tag_div.xpath("./span[@class='product-card-offer__shop']/a").text
    i = shops_from_hashes.index(shop)
    if i
      id_shop = shops_ids_from_hashes[i]
    else
      sh = Shop.find(login: shop)
      if sh
        id_shop = sh.id
      else
        shop_rating_string = tag_div.xpath("./div[@class='rating-stars-block']/div/@data-rate").text
        shop_rating = to_i_or_nil(shop_rating_string)
        rating_hint  = tag_div.xpath("./div[@class='rating-stars-block']/div/@onclick").text
        quantity_of_customer_feedback = feedback_quantity(rating_hint)
        phone = phone_number(tag_div)
        shop_info_url = product_page.xpath("//div[@class='shops-info__link']/a/@href").text
        url  = ROOT_URL + shop_info_url
        page_info = Browse.new.page(url)
        array = parse_shop_info(page_info, shop)
        login     = array[0]
        name      = array[1]
        date      = array[2]
        unp_ogrn  = array[3]
        ogrn_flag = array[4]
        address   = array[5]
        array_of_hashes_shops << { login: login, rating: shop_rating,
                                   quantity_of_customer_feedback: quantity_of_customer_feedback,
                                   name: name, registration_yandex_date: date, unp_ogrn: unp_ogrn,
                                   ogrn_flag: ogrn_flag, address: address, phone: phone }
        id_shop_index += 1
        shops_from_hashes << login
        shops_ids_from_hashes << id_shop_index
        id_shop = id_shop_index
        puts "Shop: #{name}"
      end
    end
    [array_of_hashes_shops, id_shop, id_shop_index, login, shops_from_hashes, shops_ids_from_hashes]
  end

  def parse_several_shops(array_of_hashes_shops, id_shop_index, page_attr,
                          shops, array_shops_names, one_shop_name, id_shop,
                          shops_from_hashes, shops_ids_from_hashes)
    shops_name_uniq = [one_shop_name]
    shops_ids_uniq  = [id_shop]
    new_shops = []
    shops.each do |shop|
      i = shops_from_hashes.index(shop)
      if i
        shops_name_uniq << shop
        shops_ids_uniq  << shops_ids_from_hashes[i]
      else
        sh = Shop.find(login: shop)
        if sh
          shops_name_uniq << shop
          shops_ids_uniq  << sh.id
        else
          shops_name_uniq << shop
          shops_ids_uniq  << 0
          n = shops_name_uniq.length - 1
          i = array_shops_names.index(shop)
          new_shops << [shop, i, n]
        end
      end
    end
    if new_shops != []
      tags_div = page_attr.xpath("//div[@class='product-top-offers-list__body filter-applied-results i-bem']/
                                  div/div/div[@class='product-top-offer__item product-top-offer__item_type_shop']")
      shop_rating_string_array = tags_div.xpath("./div[@class='rating-stars-block']/div/@data-rate").map(&:text)
      rating_hint_array  = tags_div.xpath("./div[@class='rating-stars-block']/div/@onclick").map(&:text)
      quantity_of_customer_feedback_array = []
      shop_rating = []
      shop_rating_string_array.each_with_index do |shop_rating_string, i|
        shop_rating << to_i_or_nil(shop_rating_string)
        quantity_of_customer_feedback_array << feedback_quantity(rating_hint_array[i])
      end
      shop_info_url = page_attr.xpath("//div[@class='shops-info__link']/a/@href").text
      url  = ROOT_URL + shop_info_url
      page_info = Browse.new.page(url)
      new_shops.each do |new_shop|
        i = new_shop[1]
        n = new_shop[2]
        array = parse_shop_info(page_info, new_shop[0])
        login     = array[0]
        name      = array[1]
        date      = array[2]
        unp_ogrn  = array[3]
        ogrn_flag = array[4]
        address   = array[5]
        array_of_hashes_shops << { login: login, rating: shop_rating[i],
                                   quantity_of_customer_feedback: quantity_of_customer_feedback_array[i],
                                   name: name, registration_yandex_date: date, unp_ogrn: unp_ogrn,
                                   ogrn_flag: ogrn_flag, address: address }
        id_shop_index += 1
        shops_from_hashes << login
        shops_ids_from_hashes << id_shop_index
        puts "Shop: #{name}"
        if shops_ids_uniq[n] == 0
          shops_ids_uniq[n] = id_shop_index
        else
          ShopClothesIdError
        end
      end
    end
    array_ids_shops = []
    array_shops_names.each do |shop_name|
      i = shops_name_uniq.index(shop_name)
      array_ids_shops << shops_ids_uniq[i]
    end
    [array_of_hashes_shops, id_shop_index, array_ids_shops, shops_from_hashes, shops_ids_from_hashes]
  end

  private

  def to_i_or_nil(string)
    if string == ''
      result = nil
    else
      result = string.to_i
    end
    result
  end

  def phone_number(tag_div)
    phone_text = tag_div.xpath("./div/div[@class='phone i-bem']/@onclick").text
    if phone_text == ''
      phone = nil
    else
      phone_text = phone_text[phone_text.rindex(%Q<,"phone":">)+10..-1]
      phone = phone_text[0..phone_text.index(%Q<">)-1]
    end
    phone
  end

  def parse_shop_info(page_info, shop)
    table = page_info.xpath("//div[h2[text()='#{shop}']]/table[@class='b-old-shop-info__table']")
    raise ShopClothesNameError if table == []
    login    = table.xpath("./tr[td[text()='Название']]/td[2]").text
    name_parsed = table.xpath("./tr[td[text()='Владелец']]/td[2]").text
    name_parsed.strip!
    name_parsed = name_parsed[3..-1] if name_parsed[0..4] == 'ИП ИП'
    name      = name_parsed
    unp       = table.xpath("./tr[td[text()='УНП']]/td[2]").text
    ogrn      = table.xpath("./tr[td[text()='ОГРН']]/td[2]").text
    ogrnip    = table.xpath("./tr[td[text()='ОГРНИП']]/td[2]").text
    address   = table.xpath("./tr[td[text()='Юридический адрес']]/td[2]").text
    reg_date  = table.xpath("./tr[last()]/td[2]").text
    date      = string_to_date(reg_date)
    array     = unp_or_ogrn(unp, ogrn, ogrnip)
    unp_ogrn  = array[0]
    ogrn_flag = array[1]
    [login, name, date, unp_ogrn, ogrn_flag, address]
  end

  def unp_or_ogrn(unp, ogrn, ogrnip)
    ogrn_flag = false
    if unp != ''
      unp_ogrn = unp.to_i
    elsif ogrn !=''
      unp_ogrn = ogrn.to_i
      ogrn_flag = true
    elsif ogrnip !=''
      unp_ogrn = ogrnip.to_i
      ogrn_flag = true
    else
      unp_ogrn  = nil
      ogrn_flag = nil
    end
    [unp_ogrn, ogrn_flag]
  end

  def feedback_quantity(rating_hint)
    case rating_hint.length
      when 0        then quantity_of_customer_feedback = nil
      when 108      then quantity_of_customer_feedback = 0
      when 131..134 then quantity_of_customer_feedback = rating_hint[62..-69].to_i
      else
        raise ShopClothesError
    end
    quantity_of_customer_feedback
  end

  def string_to_date(reg_date)
    str_date = reg_date
    year = str_date[-4..-1].to_i
    str_date = str_date[0..-5]
    day = str_date[0...str_date.index(' ')].to_i
    str_date = str_date[(str_date.index(' ') + 1)..-2]
    months = { 'января' => 1,   'февраля' => 2,  'марта' => 3,   'апреля' => 4,
               'мая' => 5,      'июня' => 6,     'июля' => 7,    'августа' => 8,
               'сентября' => 9, 'октября' => 10, 'ноября' => 11, 'декабря' => 12 }
    month = months[str_date]
    Time.mktime(year, month, day)
  end
end
