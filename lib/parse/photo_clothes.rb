# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../models/photo'

class PhotoClothes

  def id_define
    last_photo = Photo.dataset.order(:id).last
    if last_photo
      id = last_photo.id
    else
      id = 0
    end
    id
  end

  def parse(array_of_hashes_photos, id_photo, product_page, image_url, id_product)
    if image_url != ''
      array_of_hashes_photos << { product_id: id_product, url: "https:#{image_url}", first: true }
      id_photo += 1
    end
    tags_a = product_page.xpath("//li[@class='product-gallery__item']/a")
    tags_a.each do |a|
      url = a.xpath("./@href").text
      array_of_hashes_photos << { product_id: id_product, url: "https:#{url}" }
      id_photo += 1
      url = a.xpath("./img/@src").text
      array_of_hashes_photos << { product_id: id_product, url: "https:#{url}", parent_id: id_photo }
      id_photo += 1
    end
    [array_of_hashes_photos, id_photo]
  end
end