# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../models/category'
require_relative 'producer_electronics'
require_relative 'electronics'
require_relative 'clothes'

class ProductType
  def parse_manage(url, page, id_category)
    url_producers = page.xpath("//div[@class='b-category-pop-vendors']/a/@href").text
    if url_producers.empty?
      type_define(url, page, id_category)
    else
      Category.find(id: id_category).set(electronics_like: true)
      ProducerElectronics.new.parse(url_producers, id_category)
    end
  end

  private

  def type_define(url, page, id_category)
    if !page.xpath("/html/body/div[@class='main']").empty?
      Category.find(id: id_category).set(electronics_like: true)
      Electronics.new.number_of_attributes_define(url, page, id_category)
    elsif !page.xpath("/html/body/div[@class='b-max-width']").empty?
      Category.find(id: id_category).set(electronics_like: false)
      Clothes.new.parse(page, id_category)
    else
      raise ProductTypeDefineError
    end
  end
end
