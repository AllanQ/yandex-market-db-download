# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../models/producer'
require_relative '../errors/producer_electronics_id_error'
require_relative 'electronics'

class ProducerElectronics
  def parse(url, id_category)
    page   = Browse.new.page(ROOT_URL + url)
    a_tags = page.xpath("/html/body/div/table/tr/td[@class='l-page__left']/div/ul/li/a")
    names  = a_tags.map(&:text)
    urls   = a_tags.xpath("./@href").map(&:text)
    last_producer = Producer.dataset.order(:id).last
    if last_producer
      data = table_producers_not_empty(names, urls, last_producer)
    else
      data = table_producers_empty(names, urls)
    end
    data.each do |id_producer, url_producer|
      products(id_producer, url_producer, id_category)
    end
  end

  private

  def table_producers_not_empty(names, urls, last_producer)
    array_of_hashes = []
    data            = []
    id_new = last_producer.id
    names.each_with_index do |name, i|
      producer_exist = Producer.find(name: name)
      if producer_exist
        id = producer_exist.id
      else
        id_new += 1
        id = id_new
        array_of_hashes << {name: name}
      end
      data << [id, urls[i]]
    end
    if array_of_hashes != []
      Producer.dataset.multi_insert(array_of_hashes)
    end
    data
  end

  def table_producers_empty(names, urls)
    array_of_hashes = []
    data            = []
    id = 0
    names.each_with_index do |name, i|
      id += 1
      array_of_hashes << {name: name}
      data << [id, urls[i]]
    end
    Producer.dataset.multi_insert(array_of_hashes)
    id_last = Producer.dataset.order(:id).last.id
    raise ProducerElectronicsIdError if id != id_last
    data
  end

  def products(id_producer, url_producer, id_category)
    url_producer = ROOT_URL + url_producer
    page_products = Browse.new.page(url_producer)
    text = page_products.xpath("//div[@class='serp-empty island i-bem serp-empty_state_shown']/div/h1").text
    if text != 'Нет подходящих товаров'
      Electronics.new.number_of_attributes_define(url_producer, page_products, id_category, id_producer)
    end
  end
end
