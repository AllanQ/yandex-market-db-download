# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../errors/attribute_clothes_error'
require_relative '../models/attribute_name'
require_relative '../models/attribute_value'

class AttributeClothes

  def id_name_define
    last_attr_name = AttributeName.dataset.order(:id).last
    if last_attr_name
      id_attr_name_i = last_attr_name.id
    else
      id_attr_name_i = 0
    end
    id_attr_name_i
  end

  def parse_from_separate_page(page_attr, id_category, id_product, array_of_hashes_attr_names,
                               array_of_hashes_attr_values, id_attr_name,
                               attr_names_from_hashes, attr_names_ids_from_hashes)
    tags_dl  = page_attr.xpath("//div[@class='product-spec-wrap__body']/dl")
    description = page_attr.xpath("//div[@class='product-spec-wrap__desc']").text
    description = nil if description == ''
    intermediate_data(tags_dl, description, id_category, id_product, array_of_hashes_attr_names,
                      array_of_hashes_attr_values, id_attr_name,
                      attr_names_from_hashes, attr_names_ids_from_hashes)
  end

  def parse_from_product_page(page_product, id_category, id_product, array_of_hashes_attr_names,
                              array_of_hashes_attr_values, id_attr_name,
                              attr_names_from_hashes, attr_names_ids_from_hashes)
    product_card  = page_product.xpath("//div[class='product-card__spec']")
    tags_dl       = product_card.xpath("./dl")
    description   = product_card.xpath("./div[@class='product-card__spec-text']").text
    discount_info = product_card.xpath("./div[class='discount-info']").text
    if description == '' && discount_info == ''
      description = nil
    elsif description == '' && discount_info != ''
      description = discount_info
    elsif description != '' && discount_info != ''
      description = "#{description} | #{discount_info}"
    end
    intermediate_data(tags_dl, description, id_category, id_product, array_of_hashes_attr_names,
                      array_of_hashes_attr_values, id_attr_name,
                      attr_names_from_hashes, attr_names_ids_from_hashes)
  end

  private

  def intermediate_data(tags_dl, description, id_category, id_product, array_of_hashes_attr_names,
                        array_of_hashes_attr_values, id_attr_name,
                        attr_names_from_hashes, attr_names_ids_from_hashes)
    names         = ['Описание']
    arrays_values = [[description]]
    tags_dl.each do |dl|
      array = detailed_parse(dl)
      name         = array[0]
      values_array = array[1]
      names         << name
      arrays_values << values_array
    end
    data = names.zip(arrays_values)
    fill_db(data, id_category, id_product, array_of_hashes_attr_names,
            array_of_hashes_attr_values, id_attr_name,
            attr_names_from_hashes, attr_names_ids_from_hashes)
  end

  def detailed_parse(dl)
    name = dl.xpath("./dt/span").text
    span = dl.xpath("./dd/span")
    color = span.xpath("./param/@name").text
    values_array = []
    case color
      when 'Цвет'
        divs = span.xpath("./div/div")
        divs.each do |div|
          values_array << div.xpath("./@title").text
        end
      when ''
        values_array << span.text
      else
        raise AttributeClothesError
    end
    [name, values_array]
  end

  def fill_db(data, id_category, id_product, array_of_hashes_attr_names,
              array_of_hashes_attr_values, id_attr_name_i,
              attr_names_from_hashes, attr_names_ids_from_hashes)
    data.each do |name, values_array|
      i = attr_names_from_hashes.index(name)
      if i
        id_attribute_name = attr_names_ids_from_hashes[i]
      else
        id_parent = 0
        attribute_name = AttributeName.find(category_id: id_category, parent_id: id_parent, name: name)
        if attribute_name
          id_attribute_name = attribute_name.id
        else
          array_of_hashes_attr_names << { category_id: id_category, parent_id: id_parent, name: name }
          id_attr_name_i += 1
          id_attribute_name = id_attr_name_i
          attr_names_from_hashes     << name
          attr_names_ids_from_hashes << id_attr_name_i
        end
      end
      values_array.each do |value|
        # value.gsub!("\n", '')
        # value.gsub!("  ", '')
        # value.strip!
        array_of_hashes_attr_values << { product_id: id_product, attributes_names_id: id_attribute_name, value: value }
      end
    end
    [array_of_hashes_attr_names, array_of_hashes_attr_values, id_attr_name_i,
     attr_names_from_hashes, attr_names_ids_from_hashes]
  end
end
