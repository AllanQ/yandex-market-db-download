# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'csv'
require 'benchmark'
require 'pry'
require_relative 'product_type'
require_relative '../models/category'
require_relative '../browse/browse'

class CategoryParse
  def parse(url, page, id_category)
    tag_main = page.xpath("/html/body/div[@class='main']")
    tag_div  = tag_main.xpath("./div/div/div/div")
    flag = !tag_div.empty?
    if flag && ( (!tag_div.xpath("./a[@class='link catalog-menu__title metrika i-bem']").empty?) ||
        (!tag_div.xpath("./a[@class='link catalog-menu__title catalog-menu__title_type_more metrika i-bem']").empty?) ||
        (!tag_div.xpath("./a[@class='catalog-menu__title metrika i-bem']").empty?) )
      detailed_parse(tag_div, id_category)
    else
      ProductType.new.parse_manage(url, page, id_category)
    end
  end

  private

  def detailed_parse(tag_div, id_category)
    parent_id   = id_category
    names       = []
    urls_or_div = []
    tag_div.map do |div|
      a = div.xpath("./a")
      name = a.text
      next if name == ''
      url  = a.xpath("./@href").text
      names       << name
      urls_or_div << (url.empty? ? div : url)
    end
    data = names.zip(urls_or_div)
    data.each do |name, url_or_div|
      print parent_id
      print ' '
      print name
      print ' '
      if url_or_div.class == String
        puts url_or_div
      else
        puts
      end
      puts '*' * 75
      parse_down(name, url_or_div, parent_id)
    end
  end

  def parse_down(name, url_or_div, parent_id)
    if url_or_div.class == String
      url       = ROOT_URL + url_or_div
      category  = Category.new(name: name, parent_id: parent_id, url: url)
      category.save
      next_id   = category.id
      next_page = Browse.new.page(url)
      parse(url, next_page, next_id)
    else
      category = Category.new(name: name, parent_id: parent_id)
      category.save
      next_id  = category.id
      div      = url_or_div
      parse_categories_without_href(next_id, div)
    end
  end

  def parse_categories_without_href(parent_id, div)
    array_a_tags = div.xpath(".//a[@class='link catalog-menu__list-item metrika i-bem']")
    names = array_a_tags.map(&:text)
    urls  = array_a_tags.xpath("./@href").map(&:text)
    data  = names.zip(urls)
    data.each do |name, url_or_div|
      parse_down(name, url_or_div, parent_id)
    end
  end
end
