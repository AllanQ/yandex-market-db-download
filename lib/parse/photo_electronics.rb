# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../models/photo'

class PhotoElectronics

  def several(page, array_product_id)
    product_id = array_product_id[0]
    id_photo = id_define
    array_of_hashes_photos = []
    urls_all_attributes    = []
    urls = page.xpath("//a[@class='snippet-card__header-link link']/@href").map(&:text)
    urls.each do |url|
      product_id  += 1
      url_product  = ROOT_URL + url
      product_page = Browse.new.page(url_product)
      tags_a = product_page.xpath("//li[@class='product-card-gallery__thumbs-item']/a")
      tags_a.each do |a|
        url = a.xpath("./@href").text
        array_of_hashes_photos << { product_id: product_id, url: "https:#{url}" }
        id_photo += 1
        url = a.xpath("./img/@src").text
        array_of_hashes_photos << { product_id: product_id, url: "https:#{url}", parent_id: id_photo }
        id_photo += 1
      end
      urls_all_attributes << ROOT_URL + product_page.xpath("//div[@class='product-card__spec-text-more']/a/@href").text
    end
    raise PhotoElectronicsProductIdError if product_id != array_product_id[1]
    Photo.dataset.multi_insert(array_of_hashes_photos)
    urls_all_attributes
  end

  def one_from_product(page, array_product_id)
    product_id = array_product_id[0]
    urls = page.xpath("//a[@class='snippet-card__image link']/img/@src").map(&:text)
    array_of_hashes_photos = []
    urls.each do |url|
      product_id += 1
      array_of_hashes_photos << { product_id: product_id, url: "https:#{url}", first: true }
    end
    raise PhotoElectronicsProductIdError if product_id != array_product_id[1]
    Photo.dataset.multi_insert(array_of_hashes_photos)
  end

  def one_from_shop(page, a_lot_of_attributes, array_product_id, shops_ids, id_product = nil)
    tags_a = page.xpath("//a[@class='snippet-card__image shop-history__link link']")
    if a_lot_of_attributes
      product_id = id_product
    else
      product_id = array_product_id[0]
    end
    array_of_hashes_photos = []
    tags_a.each_with_index do |a, i|
      product_id += 1 if !a_lot_of_attributes
      url = a.xpath("./img[@class='image']/@src").text
      if url != ''
        array_of_hashes_photos << { product_id: product_id, shop_id: shops_ids[i], url: "https:#{url}" }
      end
    end
    Photo.dataset.multi_insert(array_of_hashes_photos)
  end

  private

  def id_define
    last_photo = Photo.dataset.order(:id).last
    if last_photo
      id = last_photo.id
    else
      id = 0
    end
    id
  end
end
