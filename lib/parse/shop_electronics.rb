# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../models/shop'
require_relative '../browse/browse'
require_relative '../errors/shop_electronics_error'
require_relative '../errors/shop_electronics_id_error'

class ShopElectronics

  def parse(page)
    shops = page.xpath("//a[@class='link link_outer_yes link_type_shop-name shop-history__link']").map(&:text)
    shops_ids = []
    new_shops = []
    shops.each_with_index do |shop, i|
      sh = Shop.find(login: shop)
      if sh
        shops_ids << sh.id
      else
        shops_ids << 0
        n = shops_ids.length - 1
        new_shops << [shop, i, n]
      end
    end
    if new_shops != []
      array = parse_shop_info(page)
      logins     = array[0]
      names      = array[1]
      dates      = array[2]
      unps_ogrns = array[3]
      ogrn_flags = array[4]
      addresses  = array[5]
      divs_ratings_stars_block = page.xpath("//div[@class='snippet-card__shop']/div[@class='rating-stars-block']")
      shop_last = Shop.dataset.order(:id).last
      if shop_last
        id_new = shop_last.id
      else
        id_new = 0
      end
      array_of_hashes_shops = []
      new_shops.each do |new_shop|
        id_new += 1
        j = logins.index(new_shop[0])
        i = new_shop[1]
        n = new_shop[2]
        shops_rating_string = divs_ratings_stars_block[i].xpath("./div/@data-rate").text
        shops_rating = to_i_or_nil(shops_rating_string)
        rating_hint  = divs_ratings_stars_block[i].xpath("./div/@onclick").text
        quantity_of_customer_feedback = feedback_quantity(rating_hint)
        array_of_hashes_shops << { login: logins[j], rating: shops_rating,
                                   quantity_of_customer_feedback: quantity_of_customer_feedback,
                                   name: names[i], registration_yandex_date: dates[i], unp_ogrn: unps_ogrns[i],
                                   ogrn_flag: ogrn_flags[i], address: addresses[i] }
        puts "Shop: #{names[i]}"
        if shops_ids[n] == 0
          shops_ids[n] = id_new
        else
          raise ShopElectronicsIdError
        end
      end
      Shop.dataset.multi_insert(array_of_hashes_shops)
    end
    shops_ids
  end

  private

  def parse_shop_info(page)
    shop_info_url = page.xpath("//div[@class='shops-info__link']/a/@href").text
    url  = ROOT_URL + shop_info_url
    page_info = Browse.new.page(url)
    tables = page_info.xpath("//table[@class='b-old-shop-info__table']")
    logins     = []
    names      = []
    dates      = []
    unps_ogrns = []
    ogrn_flags = []
    addresses  = []
    tables.each do |table|
      logins     << table.xpath("./tr[td[text()='Название']]/td[2]").text
      name = table.xpath("./tr[td[text()='Владелец']]/td[2]").text
      name.strip!
      name = name[3..-1] if name[0..4] == 'ИП ИП'
      name = nil if name == ''
      names      << name
      unp        = table.xpath("./tr[td[text()='УНП']]/td[2]").text
      ogrn       = table.xpath("./tr[td[text()='ОГРН']]/td[2]").text
      ogrnip     = table.xpath("./tr[td[text()='ОГРНИП']]/td[2]").text
      array      = unp_or_ogrn(unp, ogrn, ogrnip)
      unps_ogrns << array[0]
      ogrn_flags << array[1]
      address = table.xpath("./tr[td[text()='Юридический адрес']]/td[2]").text
      address = nil if address == ''
      addresses  << address
      reg_date   = table.xpath("./tr[last()]/td[2]").text
      dates      << string_to_date(reg_date)
    end
    [logins, names, dates, unps_ogrns, ogrn_flags, addresses]
  end

  def unp_or_ogrn(unp, ogrn, ogrnip)
    ogrn_flag = false
    if unp != ''
      unp_ogrn = unp.to_i
    elsif ogrn !=''
      unp_ogrn = ogrn.to_i
      ogrn_flag = true
    elsif ogrnip !=''
      unp_ogrn = ogrnip.to_i
      ogrn_flag = true
    else
      unp_ogrn  = nil
      ogrn_flag = nil
    end
    [unp_ogrn, ogrn_flag]
  end

  def feedback_quantity(rating_hint)
    case rating_hint.length
      when 0        then quantity_of_customer_feedback = nil
      when 108      then quantity_of_customer_feedback = 0
      when 131..134 then quantity_of_customer_feedback = rating_hint[62..-69].to_i
      else
        raise ShopElectronicsError
    end
    quantity_of_customer_feedback
  end

  def string_to_date(reg_date_j)
    str_date = reg_date_j
    year = str_date[-4..-1].to_i
    str_date = str_date[0..-5]
    day = str_date[0...str_date.index(' ')].to_i
    str_date = str_date[(str_date.index(' ') + 1)..-2]
    months = { 'января' => 1,   'февраля' => 2,  'марта' => 3,   'апреля' => 4,
               'мая' => 5,      'июня' => 6,     'июля' => 7,    'августа' => 8,
               'сентября' => 9, 'октября' => 10, 'ноября' => 11, 'декабря' => 12 }
    month = months[str_date]
    Time.mktime(year, month, day)
  end

  def to_i_or_nil(string)
    if string == ''
      result = nil
    else
      result = string.to_i
    end
    result
  end
end
