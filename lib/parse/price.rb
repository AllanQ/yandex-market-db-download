# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../errors/price_product_id_error'
require_relative 'shop_electronics'
require_relative 'supply_electronics'
require_relative 'description_electronics'
require_relative 'photo_electronics'

class Price

  def parse(page, a_lot_of_attributes, id_attribute_name_description, array_product_id)
    id_product = array_product_id[0]
    urls_prices = page.xpath("//a[@class='button button_size_s button_theme_blue smart-link i-bem']/@href").map(&:text)
    urls_prices.each do |url_price|
      id_product += 1
      url = ROOT_URL + url_price
      price_page = Browse.new.page(url)
      flag_next_price_page = parse_one_page(price_page, a_lot_of_attributes, id_product, id_attribute_name_description)
      if flag_next_price_page
        page_number = 1
        while flag_next_price_page
          page_number += 1
          price_next_page_url  = "#{url}&page=#{page_number}"
          next_price_page      = Browse.new.page(price_next_page_url)
          flag_next_price_page = parse_one_page(next_price_page, a_lot_of_attributes, id_product,
                                                id_attribute_name_description)
        end
      end
    end
    id_product_last = array_product_id[1]
    raise PriceProductIdError if id_product != id_product_last
  end

  private

  def parse_one_page(price_page, a_lot_of_attributes, id_product, id_attribute_name_description)
    shops_ids = ShopElectronics.new.parse(price_page)
    array_product_id = nil
    SupplyElectronics.new.parse(price_page, a_lot_of_attributes, array_product_id, shops_ids, id_product)
    DescriptionElectronics.new.attribute_value(price_page, a_lot_of_attributes, id_attribute_name_description,
                                                           array_product_id, shops_ids, id_product)
    PhotoElectronics.new.one_from_shop(price_page, a_lot_of_attributes, array_product_id, shops_ids, id_product)
    !price_page.xpath("//
               a[@class='button button_size_s button_theme_pseudo button_side_right button_type_arrow  i-bem ']").empty?
  end

end