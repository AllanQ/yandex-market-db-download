# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative 'electronics_parse_down'
require_relative 'description_electronics'
require_relative 'shop_electronics'
require_relative 'supply_electronics'
require_relative 'price'
require_relative 'photo_electronics'
require_relative 'attribute_electronics'
require_relative '../browse/browse'
require_relative '../errors/electronics_producer_error'
require_relative '../errors/electronics_attributes_error'

class Electronics

  def number_of_attributes_define(url, page, id_category, id_producer = nil)
    product_url = page.xpath("//div[@class='snippet-card__price']")[0].text
    if (product_url.empty? || product_url.include?('//market-click2.yandex.ru/redir/'))
      a_lot_of_attributes = false
    else
      a_lot_of_attributes = true
    end
    parse(url, page, id_category, id_producer, a_lot_of_attributes)
  end

  private

  def parse(url, page, id_category, id_producer, a_lot_of_attributes)
    id_attribute_name_description = DescriptionElectronics.new.attribute_name(id_category)
    flag_next_page = parse_one_page(page, id_category, id_producer, a_lot_of_attributes,
                                    id_attribute_name_description)
    if flag_next_page
      page_number = 1
      while flag_next_page
        page_number   += 1
        next_page_url  = "#{url}&page=#{page_number}"
        next_page      = Browse.new.page(next_page_url)
        flag_next_page = parse_one_page(next_page, id_category, id_producer, a_lot_of_attributes,
                                        id_attribute_name_description)
      end
    end
  end

  def parse_one_page(page, id_category, id_producer, a_lot_of_attributes, id_attribute_name_description)
    array_product_id = ElectronicsParseDown.new.product_parse(page, id_category, a_lot_of_attributes, id_producer)
    if a_lot_of_attributes
      a_lot_of_attributes_parse(page, a_lot_of_attributes, id_attribute_name_description,
                                array_product_id, id_category)
    else
      several_attributes_parse(page, a_lot_of_attributes, id_attribute_name_description, array_product_id)
    end
    !page.xpath("//
               a[@class='button button_size_s button_theme_pseudo button_side_right button_type_arrow  i-bem ']").empty?
  end

  def several_attributes_parse(page, a_lot_of_attributes, id_attribute_name_description, array_product_id)
    shops_ids = ShopElectronics.new.parse(page)
    SupplyElectronics.new.parse(page, a_lot_of_attributes, array_product_id, shops_ids)
    PhotoElectronics.new.one_from_shop(page, a_lot_of_attributes, array_product_id, shops_ids)
    DescriptionElectronics.new.attribute_value(page, a_lot_of_attributes, id_attribute_name_description,
                                                           array_product_id, shops_ids)
  end

  def a_lot_of_attributes_parse(page, a_lot_of_attributes, id_attribute_name_description,
                                array_product_id, id_category)
    PhotoElectronics.new.one_from_product(page, array_product_id)
    Price.new.parse(page, a_lot_of_attributes, id_attribute_name_description, array_product_id)
    urls_all_attributes = PhotoElectronics.new.several(page, array_product_id)
    AttributeElectronics.new.parse(array_product_id, urls_all_attributes, id_category)
  end
end
