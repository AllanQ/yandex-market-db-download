# encoding: utf-8
require 'nokogiri'
require 'sequel'
require 'benchmark'
require 'pry'
require_relative '../browse/browse'
require_relative '../models/attribute_name'
require_relative '../models/attribute_value'
require_relative '../errors/attribute_electronics_product_id_error'

class AttributeElectronics

  def parse(array_product_id, urls_all_attributes, id_category)
    product_id = array_product_id[0]
    urls_all_attributes.each do |url|
      product_id = parse_one_page(url, product_id, id_category)
    end
    raise AttributeElectronicsProductIdError if product_id != array_product_id[1]
  end

  private

  def parse_one_page(url, product_id, id_category)
    product_id += 1
    page = Browse.new.page(url)
    divs = page.xpath("//div[@class='product-spec-wrap__body']")
    main_titles = []
    titles      = []
    values      = []
    hints       = []
    divs.each do |div|
      main_title = div.xpath("./h2").text
      next if (!main_title || main_title == '')
      main_titles << main_title
      dt_names  = div.xpath("./dl/dt")
      some_titles   = []
      some_hints   = []
      dt_names.each do |dt|
        some_titles += [dt.xpath("./span/text()[position()=1]").text]
        hint = dt.xpath("./span/div/div/div/div").text
        hint = nil if hint == ''
        some_hints  += [hint]
      end
      titles << some_titles
      hints  << some_hints
      values << div.xpath("./dl/dd/span").map(&:text)
    end
    title_last = AttributeName.dataset.order(:id).last
    if title_last
      id = title_last.id
    else
      id = 0
    end
    array_of_hashes_main_titles = []
    array_of_hashes_titles      = []
    array_of_hashes_values      = []
    main_titles.each_with_index do |main_title, i|
      main_title_db = AttributeName.find(parent_id: 0, name: main_title)
      if main_title_db
        id_main_title = main_title_db.id
        titles[i].each_with_index do |title, j|
          title_db = AttributeName.find(parent_id: id_main_title, name: title)
          if title_db
            id_title = title_db.id
            array_of_hashes_values << { product_id: product_id, attributes_names_id: id_title,
                                        value: values[i][j] }
          else
            array_of_hashes_titles << { category_id: id_category, parent_id: id_main_title,
                                        name: title, hint: hints[i][j] }
            id += 1
            id_title = id
            array_of_hashes_values << { product_id: product_id, attributes_names_id: id_title,
                                        value: values[i][j] }
          end
        end
      else
        array_of_hashes_main_titles << { category_id: id_category, parent_id: 0, name: main_title }
        id += 1
        id_main_title = id
        titles[i].each_with_index do |title, j|
          array_of_hashes_titles << { category_id: id_category, parent_id: id_main_title,
                                      name: title, hint: hints[i][j] }
          id += 1
          id_title = id
          array_of_hashes_values << { product_id: product_id, attributes_names_id: id_title,
                                      value: values[i][j] }
        end
      end
    end
    AttributeName.dataset.multi_insert(array_of_hashes_main_titles) if array_of_hashes_main_titles != []
    AttributeName.dataset.multi_insert(array_of_hashes_titles) if array_of_hashes_titles != []
    AttributeValue.dataset.multi_insert(array_of_hashes_values)
    product_id
  end
end
