require 'open-uri'
require 'nokogiri'
require_relative '../browse/browse'
require_relative '../errors/captcha_error'

class Captcha

  def captcha_save(url)
    page = Browse.new.page_for_captcha(url)
    image_url = page.xpath("/html/body/div/div/div/div/form/img/@src").text
    file_name = (0...8).map { ('a'..'z').to_a[rand(26)] }.join
    File.open("lib/captcha/captcha_image/#{file_name}.gif", 'wb') do |file|
      image = Browse.new.page_curl(image_url)
      file << image.body
    end

    raise CaptchaError

  end
end