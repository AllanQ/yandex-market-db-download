class SupplyClothesSeveralArraysLengthError < StandardError
  def initialize(msg = "Arrays length error")
    super
  end
end