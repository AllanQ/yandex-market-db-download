class SupplyClothesDetailedParseError < StandardError
  def initialize(msg = "Supply clothes attribute error")
    super
  end
end