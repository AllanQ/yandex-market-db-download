class ShopElectronicsIdError < StandardError
  def initialize(msg = "Id error")
    super
  end
end