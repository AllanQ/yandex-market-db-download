class BrowsePageUrlEmptyError < StandardError
  def initialize(msg = "Url is empty")
    super
  end
end