class CaptchaError < StandardError
  def initialize(msg = "Parcer can't overcome captcha")
    super
  end
end