class AttributeClothesError < StandardError
  def initialize(msg = "Design of page clothes attribute error")
    super
  end
end