class ProducerElectronicsIdError < StandardError
  def initialize(msg = "Producer id error")
    super
  end
end