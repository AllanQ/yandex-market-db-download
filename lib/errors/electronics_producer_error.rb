class ElectronicsProducerError < StandardError
  def initialize(msg = "Producer name mast be nil")
    super
  end
end