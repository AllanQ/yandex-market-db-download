class ShopClothesIdError < StandardError
  def initialize(msg = "Id error")
    super
  end
end