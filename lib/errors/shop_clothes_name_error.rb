class ShopClothesNameError < StandardError
  def initialize(msg = "Name error")
    super
  end
end