class SupplyElectronicsShopIdError < StandardError
  def initialize(msg = "Shop id error")
    super
  end
end