class DescriptionElectronicsShopIdError < StandardError
  def initialize(msg = "Shop id error")
    super
  end
end