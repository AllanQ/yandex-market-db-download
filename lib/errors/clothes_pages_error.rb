class ClothesPagesError < StandardError
  def initialize(msg = "Error in next page number define")
    super
  end
end