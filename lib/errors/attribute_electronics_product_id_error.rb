class AttributeElectronicsProductIdError < StandardError
  def initialize(msg = "Product id error")
    super
  end
end