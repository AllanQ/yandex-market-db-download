class ProductTypeDefineError < StandardError
  def initialize(msg = "Unknown html page design")
    super
  end
end