require 'benchmark'
require 'json'
require_relative 'parse/main_category'

class StartParse
  def initialize
    $cookies_hash = {}
    $cookies_hash = JSON.parse(File.read('cookie.json'))
    $errors       = []
  end

  def parse
    Benchmark.bm do |bm|
      bm.report do
        puts"\e[H\e[2J"
        MainCategory.new.parse_manage
        at_exit do
          File.open('cookie.json', 'w') { |f| f.write($cookies_hash.to_json) }
        end
      end
    end
  end
end
