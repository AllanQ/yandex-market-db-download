require 'pg'
require 'sequel'

class Category < Sequel::Model(:categories)
  many_to_one :parent, class: self
  one_to_many :children, key: :parent_id, class: self

  one_to_many :products, key: :category_id
  one_to_many :attributes_names, key: :category_id

  plugin :validation_helpers
  def validate
    super
    validates_presence [:name, :parent_id], message: %q(can't be empty)
    validates_type String, :name
    validates_type Fixnum, :parent_id
  end
end