require 'pg'
require 'sequel'

class AttributeName < Sequel::Model(:attributes_names)
  many_to_one :parent, class: self
  one_to_many :children, key: :parent_id, class: self

  many_to_one :categories, key: :category_id
  one_to_many :attributes_values, key: :attributes_names_id

  plugin :validation_helpers
  def validate
    super
    validates_presence [:category_id, :parent_id, :name], message: %q(can't be empty)
    validates_type String, :name
    validates_type Fixnum, :parent_id
  end
end