require 'pg'
require 'sequel'

class Product < Sequel::Model(:products)
  many_to_one :categories, key: :category_id
  many_to_one :producers, key: :producer_id
  one_to_many :attributes_values, key: :product_id
  one_to_many :supplies, key: :product_id
  one_to_many :photos, key: :product_id

  plugin :validation_helpers
  def validate
    super
    validates_presence [:name, :category_id, :a_lot_of_attributes], message: %q(can't be empty)
    validates_type String,    :name
    validates_type TrueClass, :a_lot_of_attributes
  end
end