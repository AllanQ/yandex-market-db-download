require 'pg'
require 'sequel'

class Supply < Sequel::Model(:supply)
  many_to_one :shops, key: :shop_id
  many_to_one :products, key: :product_id

  plugin :validation_helpers
  def validate
    super
    validates_presence [:product_id, :shop_id, :price], message: %q(can't be empty)
    validates_type Fixnum, :price
  end
end