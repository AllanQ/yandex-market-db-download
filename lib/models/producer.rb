require 'pg'
require 'sequel'

class Producer < Sequel::Model(:producers)
  one_to_many :products, key: :product_id

  plugin :validation_helpers
  def validate
    super
    validates_presence :name, message: %q(can't be empty)
    validates_type String, :name
  end
end
