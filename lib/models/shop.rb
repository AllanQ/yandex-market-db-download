require 'pg'
require 'sequel'

class Shop < Sequel::Model(:shops)
  one_to_many :supply, key: :shop_id
  one_to_many :photos, key: :shop_id
  one_to_many :attributes_values, key: :shop_id

  plugin :validation_helpers
  def validate
    super
    validates_presence [:login, :name, :registration_yandex_date, :unp_ogrn],
                       message: %q(can't be empty)
    validates_type String, [:login, :name]
    validates_type Fixnum, [:rating, :quantity_of_customer_feedback, :unp_ogrn]
    validates_type Date,    :registration_yandex_date
  end
end