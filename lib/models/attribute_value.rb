require 'pg'
require 'sequel'

class AttributeValue < Sequel::Model(:attributes_values)
  many_to_one :products, key: :product_id
  many_to_one :attributes_names, key: :attributes_names_id
  many_to_one :shops, key: :shop_id

  plugin :validation_helpers
  def validate
    super#, :value
    validates_presence [:product_id, :attributes_names_id], message: %q(can't be empty)
    validates_type String, :value
  end
end