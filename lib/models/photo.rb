require 'pg'
require 'sequel'

class Photo < Sequel::Model(:photos)
  many_to_one :parent, class: self
  one_to_many :children, key: :parent_id, class: self

  many_to_one :products, key: :product_id
  many_to_one :shops, key: :shop_id

  plugin :validation_helpers
  def validate
    super
    validates_presence [:product_id, :url], message: %q(can't be empty)
    validates_type String, :url
  end
end