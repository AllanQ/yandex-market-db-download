Sequel.migration do
  change do
    create_table(:attributes_names) do
      primary_key :id
      foreign_key :category_id, :categories, null: false
      Fixnum      :parent_id, null: false
      String      :name, null: false
      String      :hint
    end
  end
end