Sequel.migration do
  change do
    create_table(:attributes_values) do
      primary_key :id
      foreign_key :product_id, :products, null: false
      foreign_key :shop_id, :shops
      foreign_key :attributes_names_id, :attributes_names, null: false
      String      :value#, null: false
    end
  end
end