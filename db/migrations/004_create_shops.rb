Sequel.migration do
  change do
    create_table(:shops) do
      primary_key :id
      String      :login, null: false
      Fixnum      :rating, size: 1
      Fixnum      :quantity_of_customer_feedback
      String      :name, null: false
      Date        :registration_yandex_date, null: false
      Bignum      :unp_ogrn
      FalseClass  :ogrn_flag
      String      :address
      String      :phone
    end
  end
end