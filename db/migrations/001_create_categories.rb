Sequel.migration do
  change do
    create_table(:categories) do
      primary_key :id
      String      :name, null: false
      Fixnum      :parent_id, null: false
      String      :url
      TrueClass   :electronics_like
    end
  end
end
