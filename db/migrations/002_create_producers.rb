Sequel.migration do
  change do
    create_table(:producers) do
      primary_key :id
      String      :name, null: false
    end
  end
end