Sequel.migration do
  change do
    create_table(:photos) do
      primary_key :id
      foreign_key :product_id, :products, null: false
      foreign_key :shop_id, :shops
      String      :url, null: false
      FalseClass  :first
      Fixnum      :parent_id
    end
  end
end