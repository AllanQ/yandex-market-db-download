Sequel.migration do
  change do
    create_table(:products) do
      primary_key :id
      foreign_key :category_id, :categories, null: false
      foreign_key :producer_id, :producers
      String      :name, null: false
      Float       :rating, size: [1, 1]
      FalseClass  :new_one
      TrueClass   :a_lot_of_attributes, null: false
      # TrueClass   :in_the_market, null: false
    end
  end
end