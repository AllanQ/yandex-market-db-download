Sequel.migration do
  change do
    create_table(:supply) do
      primary_key :id
      foreign_key :product_id, :products, null: false
      foreign_key :shop_id, :shops, null: false
      String      :shop_product_name
      String      :shop_product_description
      Fixnum      :price, null: false
      Fixnum      :old_price
      Fixnum      :delivery_cost
      String      :delivery
      TrueClass   :in_stock
      String      :self_delivery
      FalseClass  :self_delivery_possibility
      String      :delivery_description
    end
  end
end