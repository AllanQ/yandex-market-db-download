#!/usr/bin/env ruby
# encoding: utf-8
require 'pg'
require 'sequel'
require 'csv'
require 'yaml'
Sequel.extension :migration

# tee        ./download-site.rb > >(tee stdout.log) 2> >(tee stderr.log >&2)
File.open('errors.txt', 'w') {}

if !File.exist?('sleep-captcha-time.csv')
  CSV.open('sleep-captcha-time.csv', 'wb') do |csv|
    csv << ['Time.now', 'time', 'i', 'url_0', 'url_2']
  end
end

# if !File.exist?('benchmark.csv')
#   CSV.open('benchmark.csv', 'wb') do |csv|
#     csv << ['time', 'class', 'def']
#   end
# end

ROOT_URL = 'https://market.yandex.by'

if File.exist?('config/database.yml')
  connect_params = YAML.load_file('config/database.yml')
  DB = Sequel.postgres(connect_params)
  # DB.drop_table(:categories, :producers, :products, :shops, :supply, :photos,
  #               :attributes_names, :attributes_values, :cascade=>true)
  Sequel::Migrator.run(DB, 'db/migrations', target: 0)
  Sequel::Migrator.run(DB, 'db/migrations')
  require_relative 'lib/start_parse'
  begin
    t = StartParse.new.parse
  rescue => error
    raise
  ensure
    if error
      File.open('my_error', 'w') do |f|
        f.write("#{Time.now}\n")
        f.write("#{error.class}\n")
        f.write("#{error.message}\n")
        f.write("#{error.message}\n")
        f.write("#{error.backtrace.inspect}\n")
      end
    end
  end
  File.open('time', 'a') do |f|
    f.write(Time.now)
    f.write(t)
  end
else
  puts 'Ошибка подключения к базе данных'
end
